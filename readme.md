# Barcode

Framework free barcode generator


## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `salamek/barcode`.

    "require": {
		"salamek/barcode": "@dev"
	}

Next, update Composer from the Terminal:

    composer update




generator in html, png embedded base64 code and SVG canvas

    use Salamek\Barcode\DNS2D;
    use Salamek\Barcode\DNS1D;

    $dns1d = new DNS1D('./tmp/');
    $dns2d = new DNS2D('./tmp/');
    echo $dns1d->getBarcodeSVG("4445645656", "PHARMA2T");
    echo $dns1d->getBarcodeHTML("4445645656", "PHARMA2T");
    echo '<img src="data:image/png,' . $dns1d->getBarcodePNG("4", "C39+") . '" alt="barcode"   />';
    echo $dns1d->getBarcodePNGPath("4445645656", "PHARMA2T");
    echo '<img src="data:image/png,' . $dns1d->getBarcodePNG("4", "C39+") . '" alt="barcode"   />';



    echo $dns1d->getBarcodeSVG("4445645656", "C39");
    echo $dns2d->getBarcodeHTML("4445645656", "QRCODE");
    echo $dns2d->getBarcodePNGPath("4445645656", "PDF417");
    echo $dns2d->getBarcodeSVG("4445645656", "DATAMATRIX");
    echo '<img src="data:image/png,' . $dns2d->getBarcodePNG("4", "PDF417") . '" alt="barcode"   />';


## Width and Height example

    echo $dns1d->getBarcodeSVG("4445645656", "PHARMA2T",3,33);
    echo $dns1d->getBarcodeHTML("4445645656", "PHARMA2T",3,33);
    echo '<img src="' . $dns1d->getBarcodePNG("4", "C39+",3,33) . '" alt="barcode"   />';
    echo $dns1d->getBarcodePNGPath("4445645656", "PHARMA2T",3,33);
    echo '<img src="data:image/png,' . $dns1d->getBarcodePNG("4", "C39+",3,33) . '" alt="barcode"   />';


## Color


    echo $dns1d->getBarcodeSVG("4445645656", "PHARMA2T",3,33,"green");
    echo $dns1d->getBarcodeHTML("4445645656", "PHARMA2T",3,33,"green");
    echo '<img src="' . $dns1d->getBarcodePNG("4", "C39+",3,33,array(1,1,1)) . '" alt="barcode"   />';
    echo $dns1d->getBarcodePNGPath("4445645656", "PHARMA2T",3,33,array(255,255,0));
    echo '<img src="data:image/png,' . $dns1d->getBarcodePNG("4", "C39+",3,33,array(1,1,1)) . '" alt="barcode"   />';


## 2D Barcodes

    echo $dns2d->getBarcodeHTML("4445645656", "QRCODE");
    echo $dns2d->getBarcodePNGPath("4445645656", "PDF417");
    echo $dns2d->getBarcodeSVG("4445645656", "DATAMATRIX");

## 1D Barcodes

    echo $dns1d->getBarcodeHTML("4445645656", "C39");
    echo $dns1d->getBarcodeHTML("4445645656", "C39+");
    echo $dns1d->getBarcodeHTML("4445645656", "C39E");
    echo $dns1d->getBarcodeHTML("4445645656", "C39E+");
    echo $dns1d->getBarcodeHTML("4445645656", "C93");
    echo $dns1d->getBarcodeHTML("4445645656", "S25");
    echo $dns1d->getBarcodeHTML("4445645656", "S25+");
    echo $dns1d->getBarcodeHTML("4445645656", "I25");
    echo $dns1d->getBarcodeHTML("4445645656", "I25+");
    echo $dns1d->getBarcodeHTML("4445645656", "C128");
    echo $dns1d->getBarcodeHTML("4445645656", "C128A");
    echo $dns1d->getBarcodeHTML("4445645656", "C128B");
    echo $dns1d->getBarcodeHTML("4445645656", "C128C");
    echo $dns1d->getBarcodeHTML("44455656", "EAN2");
    echo $dns1d->getBarcodeHTML("4445656", "EAN5");
    echo $dns1d->getBarcodeHTML("4445", "EAN8");
    echo $dns1d->getBarcodeHTML("4445", "EAN13");
    echo $dns1d->getBarcodeHTML("4445645656", "UPCA");
    echo $dns1d->getBarcodeHTML("4445645656", "UPCE");
    echo $dns1d->getBarcodeHTML("4445645656", "MSI");
    echo $dns1d->getBarcodeHTML("4445645656", "MSI+");
    echo $dns1d->getBarcodeHTML("4445645656", "POSTNET");
    echo $dns1d->getBarcodeHTML("4445645656", "PLANET");
    echo $dns1d->getBarcodeHTML("4445645656", "RMS4CC");
    echo $dns1d->getBarcodeHTML("4445645656", "KIX");
    echo $dns1d->getBarcodeHTML("4445645656", "IMB");
    echo $dns1d->getBarcodeHTML("4445645656", "CODABAR");
    echo $dns1d->getBarcodeHTML("4445645656", "CODE11");
    echo $dns1d->getBarcodeHTML("4445645656", "PHARMA");
    echo $dns1d->getBarcodeHTML("4445645656", "PHARMA2T");
